#!/usr/bin/env bash

filename="${1:-/tmp/data.csv}"

echo 'Iniciando el puerto serie' 1>&2
picocom ${2:-/dev/ttyUSB0} \
| grep --line-buffered ';' \
| tee "${filename}" &

echo 'Esperando cinco segundos a que lleguen datos' 1>&2
sleep 5s

echo 'Graficando (q para salir)' 1>&2
readlink -f "$0" \
| xargs dirname \
| sed 's,$,/data/serial.plot,' \
| xargs gnuplot -p -e "filename='${filename}'"

sleep 1s
jobs -p | xargs kill -SIGTERM
