# Leer datos por el puerto serie

    picocom /dev/ttyUSB0 | grep --line-buffered ';' | tee data.csv

Presionar ctrl-a ctrl-x para salir (ctrl-a-a ctrl-x desde `tmux`).

# Al conectar el puerto serie se reinicia

Los arduinos se reinician cada vez que se intenta leer por el puerto
serie[^1].  Esto es por la señal DTR que envían los clientes serie para
iniciar la comunicación.

Se puede deshabilitar por hardware con una resistencia[^2], o leyendo
con un cliente serie con DTR deshabilitado.

El comportamiento se puede emular presionando ctrl-a ctrl-t en `picocom`
dos veces.


[^1]: <https://arduino.stackexchange.com/a/440>

[^2]: <http://playground.arduino.cc/Main/DisablingAutoResetOnSerialConnection>
