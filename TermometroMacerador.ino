// Termómetro para Macerado de Cerveza
// Copyright (C) 2016 La Inventoría <termometro@lainventoria.com.ar>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Funcionamiento:
//
// * El programa inicia y mide temperatura
// * Cuando la temperatura llega a 65C, inicia una alarma y espera 90'
// * Si durante los 90' la temperatura sube o baja de 65C, correr una
// alarma
// * Al terminar los 90' correr una alarma

// Funcionamiento avanzado:
//
// * El programa inicia y mide temperatura
// * Mantener 35C de 0 a 5 minutos
// * Mantener 45C de 10 a 20 minutos
// * Mantener 57C de 25 a 40 minutos
// * Mantener 62C de 45 a 65 minutos
// * Mantener 68C de 70 a 90 minutos
// * Mantener 78C de 95 a 100 minutos

// Yogur
//
// * Mantener 90C por 10 minutos
// * Bajar hasta 45C

// El pin digital de cada dispositivo
#define PIN_TEMP 2
#define PIN_BUZZ 3
// Temperatura
#define TEMP 65
// Grados de tolerancia
#define TOLERANCE 1
// Tiempo de macerado en segundos
#define MASH 5400

// Segundos desde el inicio
unsigned int time;
// Segundos desde el inicio del macerado
unsigned int mashing_time;
// Momento en el que inició el macerado
unsigned int mash_started_at;
// Estamos macerando?
boolean is_mashing;
boolean alert;

// OneWire es necesario para leer datos desde el sensor térmico
#include <OneWire.h>

// Iniciar el sensor
OneWire ds(PIN_TEMP);

// Comprobar que estemos en la temperatura correcta y dentro del tiempo
// de maceración
void mashing_temperature(unsigned int now, int temp) {
  alert = (temp < (TEMP - TOLERANCE)) || (temp > (TEMP + TOLERANCE));

  // XXX: Apagamos el tono al principio de cada ciclo
  if (alert) tone(PIN_BUZZ, 5000, 1000);
}

// Termino el macerado!
void mashing_time_is_over(unsigned int now) {
  if (now > MASH) tone(PIN_BUZZ, 10000, 1000);
}

void setup() {
  // También volcamos datos al puerto serie
  // XXX: Al parecer esto espera hasta que estemos leyendo del puerto
  // serie para empezar el programa!
  Serial.begin(9600);
  // E imprimimos la cabecera del CSV
  Serial.println("time;temperature;mashing;alert");
}

void loop() {
  byte i;
  byte data[12];
  byte addr[8];
  float celsius;

  // Dejamos el chequeo de alerta para más adelante
  alert = false;
  noTone(PIN_BUZZ);

  // Si no podemos leer, esperar y volver a empezar
  // XXX: si el sensor está jodido o mal conectado, no vamos a recibir
  // datos y el Arduino va a titilar constantemente porque el loop se
  // está reiniciando acá.
  if (!ds.search(addr)) {
    ds.reset_search();
    delay(250);
    return;
  }

  // Ignorar errores de lectura
  if (OneWire::crc8(addr, 7) != addr[7]) return;

  // Acá empezamos a procesar datos
  ds.reset();
  ds.select(addr);
  // XXX: Esta sección está copiada tal cual de
  // OneWire/DS18x20_Temperature, que no explica mucho.
  ds.write(0x44, 1);
  delay(1000);

  ds.reset();
  ds.select(addr);
  ds.write(0xBE);

  // Leer 9 bytes (?)
  for (i = 0; i < 9; i++) data[i] = ds.read();

  // Convertir los datos a grados Celsius
  int16_t raw = (data[1] << 8) | data[0];
  byte cfg = (data[4] & 0x60);
  // at lower res, the low bits are undefined, so let's zero them
  if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
  else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
  else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
  //// default is 12 bit resolution, 750 ms conversion time

  // TODO: es necesario medir los grados en decimal?
  celsius = (float)raw / 16.0;
  // XXX: fin de ejemplo de OneWire


  // Macerador!

  // El tiempo actual desde que empezamos el programa
  time = millis() / 1000;

  // Si estamos macerando, comprobar que estemos en la temperatura
  // correcta
  if (is_mashing) {
    // El tiempo actual ajustado al momento en que comenzó el macerado
    mashing_time = time - mash_started_at;

    // Si la temperatura actual es distinta a la que necesitamos, enviar
    // una alerta.  Redondeamos el grado para tener margen.
    mashing_temperature(mashing_time, (int)celsius);
    mashing_time_is_over(mashing_time);
  // Si no estamos macerando, comprobamos si podemos empezar
  } else {
    if (celsius >= TEMP) {
      mash_started_at = time;
      is_mashing = true;
    }
  }

  // Imprimir un CSV con los datos recolectados
  Serial.print(time);
  Serial.print(';');
  Serial.print(celsius);
  Serial.print(';');
  Serial.print(mashing_time);
  Serial.print(';');
  Serial.println(alert);
}
