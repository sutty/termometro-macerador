reset

set title "Maceración"
set key outside left bottom

set grid
set autoscale

set datafile separator ';'

set yrange [0:100]

done = 0
bind all 'q' 'done = 1'

set boxwidth 0.99 relative
set style fill transparent solid 0.5 noborder

while (!done) {
  plot filename \
       using 0:(100 * $3) with boxes fillstyle solid lc rgb"green" title columnhead, \
    "" using 0:(100 * $4) with boxes fillstyle solid title columnhead, \
    "" using 0:2 with lines lw 3 title columnhead
  pause 5
  reread
}

