# Termómetro para maceración

![Protoboard](data/TermometroMacerador_bb.png)

## Elementos

* Arduino Nano
* Protoboard
* Resistencia de 4.7K
* Sensor de temperatura DS18B20
* Buzzer (reciclado de gabinete de PC!)

## Librerías

* OneWire

## Graficar datos

![Grafico](data/TermometroMacerador_grafico.png)

### Requisitos

* picocom
* gnuplot


    ./TermometroMacerador.sh [datos.csv] [/dev/ttyUSB0]

Donde `/dev/ttyUSB0` es el puerto serie del Arduino y `datos.csv` el
archivo de datos.  Ambos son opcionales, los valores por defecto son
`/tmp/data.csv` y `/dev/ttyUSB0` respectivamente.


## Licencia

Copyright (C) 2016 La Inventoría <termometro@lainventoria.com.ar>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

### ¿Qué significa?

* Podés usar el código en tus proyectos y modificarlo a voluntad.

* Todo tu código derivado tiene que mantener esta licencia.

* Si distribuís hardware incluyendo este firmware, tenés que publicar el
  código tal cual lo compilaste.

Es decir,

* No te apropies de este proyecto que liberamos por amor a la cerveza <3
